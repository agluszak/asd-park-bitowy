cmake_minimum_required(VERSION 3.12)
project(par)

set(CMAKE_CXX_STANDARD 17)

add_executable(par main.cpp)