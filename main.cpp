#include <iostream>
#include <vector>
#include <cmath>
#include <assert.h>
#include <array>
#include <algorithm>

using namespace std;
using vec = vector<int>;
constexpr bool DEBUG = true;
void makeDepths(int pos, int d, vec& left, vec& right, vec& depth) {
    depth.at(pos) = d;
    int l = left.at(pos);
    int r = right.at(pos);
    if (l > 0)
        makeDepths(l, d + 1, left, right, depth);
    if (r > 0)
        makeDepths(r, d + 1, left, right, depth);
}

int ipow(int base, int exp) {
    int result = 1;
    for (;;) {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}

int ancestor(int v, int h, int maxDepth, vector<vec>& links) {
    int res = v;
    int i = maxDepth;
    while (h > 0) {
        int x = ipow(2, i);
        if (x > h) {
            --i;
        } else {
            res = links.at(i).at(res);
            h = h - x;
        }
    }
    return res;
}

int lca(int u, int v, int maxDepth, vec& depth, vector<vec>& links) {
    int du = depth.at(u);
    int dv = depth.at(v);
    if (du < dv) {
        v = ancestor(v, dv - du, maxDepth, links);
        dv = depth.at(v);
    } else if (du > dv) {
        u = ancestor(u, du - dv, maxDepth, links);
        du = depth.at(u);
    }
    assert(du == dv);
    if (u == v) {
        return u;
    }
    while (maxDepth >= 0) {
        if (links.at(maxDepth).at(u) != links.at(maxDepth).at(v)) {
            u = links.at(maxDepth).at(u);
            v = links.at(maxDepth).at(v);
        }
        --maxDepth;
    }
    return links.at(0).at(u);
}

void fardown(int u, vec& left, vec& right, vec& fardownNodes, vec& fardownDist) {
    int l = left.at(u);
    int r = right.at(u);
    if (l != -1)
        fardown(l, left, right, fardownNodes, fardownDist);
    if (r != -1)
        fardown(r, left, right, fardownNodes, fardownDist);
    array<int, 3> c{u, -1, -1};
    array<int, 3> dist{0, -1, -1};
    if (l != -1) {
        c[1] = fardownNodes.at(l);
        dist[1] = fardownDist.at(l) + 1;
    }
    if (r != -1) {
        c[2] = fardownNodes.at(r);
        dist[2] = fardownDist.at(r) + 1;
    }
    int best_index = distance(dist.begin(), max_element(dist.begin(), dist.end()));
    fardownNodes[u] = c[best_index];
    fardownDist[u] = dist[best_index];
}

void
farup(int u, vec& left, vec& right, vec& parent, vec& sibling, vec& fardownNodes, vec& fardownDist, vec& farupNodes,
      vec& farupDist) {
    int p = parent.at(u);
    int s = sibling.at(u);
    array<int, 3> c{u, -1, -1};
    array<int, 3> dist{0, -1, -1};
    if (p != -1) {
        c[1] = farupNodes.at(p);
        dist[1] = farupDist.at(p) + 1;
    }
    if (s != -1) {
        c[2] = fardownNodes.at(s);
        dist[2] = fardownDist.at(s) + 2;
    }
    int best_index = distance(dist.begin(), max_element(dist.begin(), dist.end()));
    farupNodes[u] = c[best_index];
    farupDist[u] = dist[best_index];
    int l = left.at(u);
    int r = right.at(u);
    if (l != -1)
        farup(l, left, right, parent, sibling, fardownNodes, fardownDist, farupNodes, farupDist);
    if (r != -1)
        farup(r, left, right, parent, sibling, fardownNodes, fardownDist, farupNodes, farupDist);

}

void makeSiblings(int u, vec& left, vec& right, vec& sibling) {
    int l = left.at(u);
    int r = right.at(u);
    if (l != -1) {
        sibling.at(l) = r;
        makeSiblings(l, left, right, sibling);
    }
    if (r != -1) {
        sibling.at(r) = l;
        makeSiblings(r, left, right, sibling);
    }
}

int main() {
    std::ios::sync_with_stdio(false);
    unsigned int n;
    cin >> n;
    vec left(n + 1);
    vec right(n + 1);
    vec parent(n + 1);
    for (unsigned int i = 1; i < n + 1; ++i) {
        parent.at(i) = -1;
        left.at(i) = -1;
        right.at(i) = -1;
    }

    for (unsigned int i = 1; i < n + 1; ++i) {
        int a, b;
        cin >> a >> b;
        left.at(i) = a;
        right.at(i) = b;
        if (a > 0)
            parent.at(a) = i;
        if (b > 0)
            parent.at(b) = i;
    }


    vec depth(n + 1);
    makeDepths(1, 0, left, right, depth);
    vec sibling(n + 1);
    for (int i = 0; i < n + 1; ++i) {
        sibling.at(i) = -1;
    }
    makeSiblings(1, left, right, sibling);

    int iSize = (int) log2(n);
    vector<vec> links(iSize + 1);
    for (int i = 0; i < iSize + 1; ++i) {
        links.at(i) = vec(n + 1);
    }
    for (int v = 0; v < n + 1; ++v) {
        links.at(0).at(v) = parent.at(v);
    }
    for (int i = 1; i < iSize + 1; ++i) {
        for (int v = 1; v < n + 1; ++v) {
            int x = links.at(i - 1).at(v);
            if (x > 0) {
                links.at(i).at(v) = links.at(i - 1).at(x);
            } else {
                links.at(i).at(v) = -1;
            }
        }
    }

    vec fardownNodes(n + 1);
    vec fardownDist(n + 1);
    fardown(1, left, right, fardownNodes, fardownDist);

    vec farupNodes(n + 1);
    vec farupDist(n + 1);
    farup(1, left, right, parent, sibling, fardownNodes, fardownDist, farupNodes, farupDist);

    vec farNodes(n + 1);
    vec farDist(n + 1);
    for (int i = 1; i < n + 1; ++i) {
        if (farupDist.at(i) > fardownDist.at(i)) {
            farDist.at(i) = farupDist.at(i);
            farNodes.at(i) = farupNodes.at(i);
        } else {
            farDist.at(i) = fardownDist.at(i);
            farNodes.at(i) = fardownNodes.at(i);
        }
    }


    int m;
    cin >> m;
    for (int j = 0; j < m; ++j) {
        int a, d;
        cin >> a >> d;
        int dMax = farDist.at(a);
        int v = farNodes.at(a);
        if (d > dMax) {
            cout << -1 << endl;
        } else {
            int l = lca(a, v, iSize, depth, links);
            int d1 = depth.at(a) - depth.at(l);
            int d2 = depth.at(v) - depth.at(l);
            if (d <= d1) {
                cout << ancestor(a, d, iSize, links) << endl;
            } else {
                cout << ancestor(v, d2 - (d - d1), iSize, links) << endl;
            }
        }
    }
    return 0;
}